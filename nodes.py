con_groups = [[[2], [3, 6, 9], [4, 7, 10], [5]],
               [[1], [3, 4, 5], [6, 8, 10], [9]],
               [[2, 4, 5], [1, 6, 9]],
               [[2, 3, 5], [1, 7, 10]],
               [[1], [2, 3, 4], [7,  8, 9], [10]],
               [[1, 3, 9], [2, 8, 10]],
               [[1, 4, 10], [5, 9]],
               [[2, 6, 10], [5, 7, 9]],
               [[5, 7, 8], [1, 3, 6], [2], [10]],
               [[1, 4, 7], [2, 6, 8], [5], [9]]]
# Each row corresponds to one node in the picture.
# Each row contains groups of nodes connected to the current node.
# Nodes in the same group are connected through the same linear line.
