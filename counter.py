import nodes
import itertools
import time
import pprint


class TriangleFinder:
    connections = list()
    con_alignment = list()
    uniqueTriangles = list()
    nTriangles = 0

    def __init__(self, alin):
        self.con_alignment = alin

    def addTr(self, triangle):
        for tri in self.uniqueTriangles:
            if set(triangle) == set(tri):
                return 0
        else:
            self.uniqueTriangles.append(triangle)
            return 1

    def find(self):
        for i, node in enumerate(self.con_alignment):
            all_conn_i = list(itertools.chain.from_iterable(node))
            for line in node:
                for j in line:
                    all_conn_j = list(itertools.chain.from_iterable(self.con_alignment[j-1]))
                    common_points = list(set(all_conn_i).intersection(all_conn_j))
                    for point in common_points:
                        if point not in line:
                            self.nTriangles += self.addTr([i + 1, j, point])


if __name__ == "__main__":
    t0 = time.time()
    tf = TriangleFinder(nodes.con_groups)
    tf.find()
    t1 = time.time()
    elapsed_time = t1-t0
    pprint.pprint(tf.uniqueTriangles)
    print("I found " + str(tf.nTriangles) + " triangles in " + str(elapsed_time) + " seconds.")

